import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Categories } from '../Messages/Category';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getCategoriesByDomain(domain: string): Observable<Categories>{
    return this.http.get<Categories>(`${environment.hostUrlAndPort}/domain-categories/${domain}`);
  }

  getCategoriesByIp(ip: string){
    return this.http.get(`${environment.hostUrlAndPort}/ip-categories/${ip}`);
  }

  getCategoriesByDomains(domains: string[]){
    return this.http.post(`${environment.hostUrlAndPort}/domain-categories`,domains);
  }

  getCategoriesByIps(ips: string[]){
    return this.http.post(`${environment.hostUrlAndPort}/ip-categories`,ips);
  }

}
