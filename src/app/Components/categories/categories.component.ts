import { Component, OnInit } from '@angular/core';
import { CategoriesService } from 'src/app/Services/categories.service';
import { Categories } from 'src/app/Messages/Category';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.less']
})
export class CategoriesComponent implements OnInit {

  value = 'www.google.com';
  ip= '8.8.8.8';
  singleDomainCategories$ = this.ctgSrv.getCategoriesByDomain(this.value);
  singleIpCategories$ = this.ctgSrv.getCategoriesByIp(this.ip);
  constructor(private ctgSrv: CategoriesService) { }
  ngOnInit(): void {
    
  }


  onDomainInputBlur(){
    this.singleDomainCategories$ = this.ctgSrv.getCategoriesByDomain(this.value);
  }

  onIpInputBlur(){
    this.singleIpCategories$ = this.ctgSrv.getCategoriesByIp(this.ip);
  }

}
