const { exec } = require("child_process");

const dockerImageName = `keeper-api-tester`;


var buildDocker = async()=>{
    await execChildProsesAsync(`sudo docker rm $(sudo docker stop $(sudo docker ps -a -q --filter ancestor='${dockerImageName}' --format="{{.ID}}"))`);
    await execChildProsesAsync(`sudo docker build -t ${dockerImageName} .`);
    await execChildProsesAsync(`sudo docker run -p 8081:80 --detach --name keeperApiTesterInstance1 ${dockerImageName}`);
}

var execChildProsesAsync = async (command) => {
    return new Promise((resolve, rejects) => {
        exec(command, (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
            //    rejects(`error: ${error.message}`)
            }
            if (stderr) {
                console.log(`stderr: ${stderr}`);
                // rejects(`stderr: ${stderr}`)
            }
            console.log(`stdout: ${stdout}`);
            resolve(`stdout: ${stdout}`)
        });
    })

}


buildDocker();